package com.ksa.employeemanagement.service;

import com.ksa.employeemanagement.entity.Employee;
import com.ksa.employeemanagement.model.EmployeeItem;
import com.ksa.employeemanagement.model.EmployeeRequest;
import com.ksa.employeemanagement.model.EmployeeResponse;
import com.ksa.employeemanagement.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public void setEmployee(EmployeeRequest request) {
        Employee addData = new Employee();
        addData.setEmployeeId(request.getEmployeeId());
        addData.setDepartmentEnum(request.getDepartmentEnum());
        addData.setName(request.getName());
        addData.setPositionEnum(request.getPositionEnum());
        addData.setJobEnum(request.getJobEnum());
        addData.setEmploymentEnum(request.getEmploymentEnum());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setIncome(request.getIncome());
        addData.setJoinDay(request.getJoinDay());
        addData.setEndDay(request.getEndDay());

        employeeRepository.save(addData);
    }

    public List<EmployeeItem> getEmployees() {
        List<Employee> originList = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        for (Employee employee : originList) {
            EmployeeItem addItem = new EmployeeItem();
            addItem.setId(employee.getId());
            addItem.setEmployeeId(employee.getEmployeeId());
            addItem.setDepartmentEnum(employee.getDepartmentEnum().getName());
            addItem.setName(employee.getName());
            addItem.setPositionEnum(employee.getPositionEnum().getName());
            addItem.setEmploymentEnum(employee.getEmploymentEnum().getName());
            addItem.setBirthDate(employee.getBirthDate());
            addItem.setPhoneNumber(employee.getPhoneNumber());
            addItem.setAddress(employee.getAddress());
            addItem.setJoinDay(employee.getJoinDay());
            addItem.setEndDay(employee.getEndDay());

            result.add(addItem);
        }
        return result;
    }

    public EmployeeResponse getEmployee(long id) {
        Employee originData = employeeRepository.findById(id).orElseThrow();

        EmployeeResponse response = new EmployeeResponse();
        response.setId(originData.getId());
        response.setEmployeeId(originData.getEmployeeId());
        response.setName(originData.getName());
        response.setPositionEnum(originData.getPositionEnum().getName());
        response.setJobEnum(originData.getJobEnum().getName());
        response.setEmploymentEnum(originData.getEmploymentEnum().getName());
        response.setBonus(originData.getEmploymentEnum().getBonus() ? "지급 대상" : "지급 안함");
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setIncome(originData.getIncome());
        response.setJoinDay(originData.getJoinDay());
        response.setEndDay(originData.getEndDay());

        return response;
    }
}
