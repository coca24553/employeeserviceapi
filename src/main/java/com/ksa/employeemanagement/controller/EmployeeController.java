package com.ksa.employeemanagement.controller;

import com.ksa.employeemanagement.model.EmployeeItem;
import com.ksa.employeemanagement.model.EmployeeRequest;
import com.ksa.employeemanagement.model.EmployeeResponse;
import com.ksa.employeemanagement.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/information")
    public String setEmployee(@RequestBody EmployeeRequest request) {
        employeeService.setEmployee(request);
        return "등록 완료";
    }

    @GetMapping("/all")
    public List<EmployeeItem> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping("/detail/{id}")
    public EmployeeResponse getEmployee(@PathVariable long id) {
        return employeeService.getEmployee(id);
    }
}
