package com.ksa.employeemanagement.entity;

import com.ksa.employeemanagement.enums.DepartmentEnum;
import com.ksa.employeemanagement.enums.EmploymentEnum;
import com.ksa.employeemanagement.enums.JobEnum;
import com.ksa.employeemanagement.enums.PositionEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String employeeId;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private DepartmentEnum departmentEnum;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PositionEnum positionEnum;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private JobEnum jobEnum;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private EmploymentEnum employmentEnum;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private Integer income;

    @Column(nullable = false)
    private LocalDate joinDay;

    @Column
    private LocalDate endDay;
}
