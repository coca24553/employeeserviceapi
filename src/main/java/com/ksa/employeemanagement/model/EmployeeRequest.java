package com.ksa.employeemanagement.model;

import com.ksa.employeemanagement.enums.DepartmentEnum;
import com.ksa.employeemanagement.enums.EmploymentEnum;
import com.ksa.employeemanagement.enums.JobEnum;
import com.ksa.employeemanagement.enums.PositionEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeRequest {
    private String employeeId;
    @Enumerated(value = EnumType.STRING)
    private DepartmentEnum departmentEnum;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private PositionEnum positionEnum;
    @Enumerated(value = EnumType.STRING)
    private JobEnum jobEnum;
    @Enumerated(value = EnumType.STRING)
    private EmploymentEnum employmentEnum;
    private LocalDate birthDate;
    private String phoneNumber;
    private Integer income;
    private String address;
    private LocalDate joinDay;
    private LocalDate endDay;
}
