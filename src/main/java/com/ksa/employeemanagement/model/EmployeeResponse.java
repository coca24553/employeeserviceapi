package com.ksa.employeemanagement.model;

import com.ksa.employeemanagement.enums.DepartmentEnum;
import com.ksa.employeemanagement.enums.EmploymentEnum;
import com.ksa.employeemanagement.enums.JobEnum;
import com.ksa.employeemanagement.enums.PositionEnum;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeResponse {
    private Long id;
    private String employeeId;
    private String departmentEnum;
    private String name;
    private String positionEnum;
    private String jobEnum;
    private String employmentEnum;
    private String bonus;
    private LocalDate birthDate;
    private String phoneNumber;
    private Integer income;
    private String address;
    private LocalDate joinDay;
    private LocalDate endDay;
}
