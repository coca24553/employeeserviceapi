package com.ksa.employeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmploymentEnum {
    PERMANENT("정규직",true),
    CONTRACT("계약직",false);

    private final String name;
    private final Boolean bonus;
}
