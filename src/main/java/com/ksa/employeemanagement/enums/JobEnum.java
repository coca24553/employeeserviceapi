package com.ksa.employeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum JobEnum {
    CEO("대표"),
    DEPARTMENT("부서장"),
    LEADER("팀장"),
    MEMBER("팀원");

    private final String name;
}
