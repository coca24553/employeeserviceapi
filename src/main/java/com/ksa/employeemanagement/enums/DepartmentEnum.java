package com.ksa.employeemanagement.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DepartmentEnum {
    ACCOUNTING("경리부"),
    TECHNICAL_SUPPORT("기술 지원팀"),
    SALES("영업팀"),
    RD("개발팀");

    private final String name;
}
